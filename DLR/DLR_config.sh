#!/bin/sh

# copy casetable to DLR local
cp ../Data/QIAW_casetable.csv ./

# pre-processing to extract cubic ROI and save in the numpy format
python run_preprocess.py

# execute deep-prognosis model to extract DLR features
# this step must be done in jupyter notebook at this moment
