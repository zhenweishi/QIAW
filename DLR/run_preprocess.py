from subprocess import call

# extract cubic ROI and save in numpy format
def Nrrd2Array(casetable,exportDir):
    try:
        call(['python', 'preprocess.py',casetable,exportDir])
    except:
        print("Error: fail to convert nrrd image to numpy array")

if __name__=="__main__":

    casetable = '/home/zhenwei/FAIR-QIAW/Data/QIAW_casetable.csv'
    exportDir = '../deep-prognosis/contrib_src/sample_data'
    Nrrd2Array(casetable,exportDir)
