import numpy as np
import pandas as pd
import SimpleITK as sitk
from skimage import measure
from scipy import ndimage
import os
import sys

def load_mask_and_image_NRRD(image_file_path, mask_file_path):
    #
    image = sitk.ReadImage(image_file_path)
    mask = sitk.ReadImage(mask_file_path)
    # sanity check
    return image, mask
    if np.array_equal(image.GetSpacing(), mask.GetSpacing()) and np.array_equal(image.GetSize(), mask.GetSize()):
        return image,mask
    else:
        raise Exception("image and mask arrays do not match")



def get_isotrpic(data,interpMethod):
    original_spacing = data.GetSpacing()
    original_size = data.GetSize()
    new_spacing = [1, 1, 1]
    new_size = [int(round(original_size[0]*(original_spacing[0]))),
                int(round(original_size[1]*(original_spacing[1]))),
                int(round(original_size[2]*(original_spacing[2])))]

    resampleImageFilter = sitk.ResampleImageFilter()
    data_iso = resampleImageFilter.Execute(data,
                                            new_size,
                                            sitk.Transform(),
                                            interpMethod,
                                            data.GetOrigin(),
                                            new_spacing,
                                            data.GetDirection(),
                                            0,
                                            data.GetPixelIDValue())


    return data_iso

def check_nodule_count(maskData):
    # copy for safety
    copyMaskData = maskData
    # get blobs
    all_labels = measure.label(copyMaskData)
    # get number of blobs ( inlcuding background as zero
    noduleCount = all_labels.max()
    #
    # list to populate
    maskDataList = []
    maskDataListVolume = []
    # supress one mask and leave the others
    # 0 is the background - so dont deal with it
    for label in range ( 1, noduleCount + 1):

        # make an array of zeros
        tempMaskData = np.zeros((copyMaskData.shape[0],copyMaskData.shape[1],copyMaskData.shape[2]) , dtype=np.int64 )
        #
        arrays = np.where(all_labels == label)
        # just loop through lenght of one of the 3 arrays - they are all the same
        for k in range ( len(arrays[0]) ):
            tempMaskData[ arrays[0][k] ][ arrays[1][k] ][ arrays[2][k] ] = 1.0

        # when array done
        maskDataList.append(tempMaskData)
        maskDataListVolume.append( tempMaskData.sum() )

    return maskDataList,maskDataListVolume


def get_closest_slice(centroidSciPy):
    return int( centroidSciPy[0] ),int( centroidSciPy[1] ),int( centroidSciPy[2] )


def check_distance(com,size,patchHalf):
    # value out
    out = com
    half = size/2.0
    # if in first half
    if com < half:
        # greater than or equal to is ok
        if com < patchHalf:
            out = patchHalf
        # else dont change
    # if in second half
    elif com > half:
        if (size-com) < patchHalf:
            out = size - patchHalf
        # else dont change
    # else if in the middle, dont change

    # SANITY CHECK
    # equal to is ok
    if out-patchHalf < 0 or out+patchHalf > size:
        raise Exception("OUT OF RANGE")

    return out



if __name__ == '__main__':

    csv_file = sys.argv[1]
    df = pd.read_csv(csv_file, index_col = None)

    patchHalf = 75
    out_dir = sys.argv[2]

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    for idx, row in df.iterrows():
        # load mask and image
        image, mask = load_mask_and_image_NRRD(row['Image'] , row['Mask'])
        # make isotropic
        image_iso = get_isotrpic(image,sitk.sitkLinear)
        mask_iso = get_isotrpic(mask,sitk.sitkNearestNeighbor)
        # count nodules
        mask_list, mask_list_volume = check_nodule_count ( sitk.GetArrayFromImage(mask_iso) )
        # if maskList is not empty
        if mask_list_volume != []:
            # get largest mask
            idx =  np.argmax (mask_list_volume)
            mask_to_use_npy = mask_list [ idx ].astype(np.int8)
            # get centroid
            com = ndimage.measurements.center_of_mass(mask_to_use_npy)
            com = get_closest_slice(com)
            # convert image to numpy array, mask is already numpy array.
            image_iso_npy = sitk.GetArrayFromImage(image_iso).astype(np.int16)
            # get center
            comX = int ( check_distance( float(com[2]) , float(image_iso_npy.shape[2]) , patchHalf ) )
            comY = int ( check_distance( float(com[1]) , float(image_iso_npy.shape[1]) , patchHalf ) )
            comZ = int ( check_distance( float(com[0]) , float(image_iso_npy.shape[0]) , patchHalf ) )
            # save
            out_array = image_iso_npy[comZ-patchHalf:comZ+patchHalf,
                                     comY-patchHalf:comY+patchHalf,
                                     comX-patchHalf:comX+patchHalf]
            np.save(os.path.join(out_dir, row['ID']+'_'+row['Mask'][row['Mask'].find('Mask_Nrrd')+10:
                                    row['Mask'].find('.nrrd')]+".npy"), out_array )

            print("image {} done, largest mask volume: {}, center of mass: {},{},{}".format(row['ID'],
                                                                                            mask_list_volume[idx],
                                                                                            comX, comY, comZ))

        else:
            raise Exception("mask volume does not contain any volumes")
