"""
###############################
@author: zhenwei.shi, Maastro##
###############################
Usage:

"""
from __future__ import print_function

import pydicom,os
import pandas as pd
import numpy as np
import re
import glob
import yaml
import shutil
from DicomDatabase import DicomDatabase
from subprocess import call
# from pathlib import Path
import batchProcessingWithPandas
import logging

# convert DICOM image to nrrd format using plastimatch
def dcmImage2Nrrd(inputImageDir,ptid,exportDir,):
    image = os.path.join(exportDir,ptid + '_image.nrrd')
    try:
        call(['plastimatch', 'convert','--input',inputImageDir,'--output-img',image])
    except:
        print("Error: plastimatch failed to convert image to image.nrrd")
    return image

# convert DICOM specified ROIs in the RTSTRUCT file to nrrd format using plastimatch
def dcmRT2Nrrd(inputRtDir,inputImageDir,exportDir,ROIname):
    try:
        call(['plastimatch', 'convert','--input',inputRtDir,'--output-prefix',exportDir, '--prefix-format', 'nrrd',\
        '--referenced-ct',inputImageDir])
    except:
        print("Error: plastimatch failed to convert RT to mask.nrrd")

    for label in os.listdir(exportDir):
        for i in range(len(ROIname)):
            if re.search(ROIname[i],label):
                roi = ROIname[i]
                
    for label in os.listdir(exportDir):
        if not re.search(roi,label):
            if os.path.isfile(os.path.join(exportDir,label)):
                os.remove(os.path.join(exportDir,label))
            elif os.path.isdir(os.path.join(exportDir,label)):
                shutil.rmtree(os.path.join(exportDir,label))
    return roi

# def SEGJsoner(JsontemplateDir,jsonexportDir):
#     # In this study, we are using one json template for all patients, which means that it is in the data set level
#     shutil.copy2(os.path.join(JsontemplateDir,'metadata.json'),jsonexportDir)


# # export radiomic features to DICOM Structured Report (DICOM-SR)
# def ExportDicomSR(pyradiomicsdcmfile,inputImageDir,inputSEG,exportDir,featureDict,paramsyaml):
#     try:
#         call(['python',pyradiomicsdcmfile,'--input-image',inputImageDir,'--input-seg-file',inputSEG,'--output-dir',\
#         exportDir,'--features-dict',featureDict,'--parameters',paramsyaml])
#     except:
#         print('Error: Failed to convert Dicom-SR')

# # # binary package: convert nrrd to SEG.dcm using dcmqi
# def dcmNrrd2SEG(inputImageList,inputDICOMDirectory,inputMetadata,inputSEG,workingDir):
#     SEGDir = os.path.join(workingDir,'SEG')
#     if not os.path.exists(SEGDir):
#         os.mkdir(SEGDir)
#     outputDICOM = os.path.join(workingDir,'SEG',inputSEG)
#     try:
#         call(['itkimage2segimage', '--inputImageList', inputImageList,'--inputDICOMDirectory',inputDICOMDirectory,\
#               '--inputMetadata',inputMetadata, '--outputDICOM', outputDICOM])
#     except:
#         print('Error: failed to pack dcm image to SEG.dcm')

# Docker version: convert ITK nrrd images to SEG.dcm via dcmqi docker
# def dcmNrrd2SEG(inputImageList,inputDICOMDirectory,inputMetadata,inputSEG,workingDir):
#     SEGDir = os.path.join(workingDir,'SEG')
#     if not os.path.exists(SEGDir):
#         os.mkdir(SEGDir)
#     docker_path = workingDir+':/tmp'
#     inputDICOMDirectory = '/tmp/dicom/Image'
#     inputMetadata = '/tmp/Json/metadata.json'
#     outputDICOM = '/tmp/SEG/'+ inputSEG
#     try:
#         call(['sudo','docker','run', '-v', docker_path,'qiicr/dcmqi','itkimage2segimage', '--inputImageList', inputImageList,'--inputDICOMDirectory',\
#             inputDICOMDirectory,'--inputMetadata',inputMetadata, '--outputDICOM', outputDICOM])
#     except:
#         print('Error: failed to pack dcm image to SEG.dcm')
# convert SEG images to ITK nrrd images
# def dcmSEGToNRRDs(inputSEG, workingDir,patientid):
#     segmentsDir = os.path.join(workingDir,'SEG')
#     if not os.path.exists(segmentsDir):
#         os.mkdir(segmentsDir)
#     try:
#         call(['segimage2itkimage', '--inputDICOM', inputSEG, '--outputDirectory', segmentsDir,'-p', patientid])
#     except:
#         print('Error: Failed to pack SEG to nrrd image')
#     return glob.glob(os.path.join(segmentsDir,"*nrrd"))
    
    
def main():
    #-----------------create the data structure-----------
    df_combine = pd.DataFrame()
    logging.basicConfig(filename='ROI_extraction.log',level=logging.DEBUG)
    # -----------------------------------------------------------
    # initialize DICOM Database containing DICOM images and associcated RTSTRUCT files
    dicomDb = DicomDatabase()
    # walk over all DICOM files in the working directory
    dicomDb.parseFolder(datasetDir)
    # excluding some ROI structures such as patient, body and so on
    excludeStructRegex = "(Patient.*|BODY.*|Body.*|NS.*|Couch.*)"
    # ----------------------------------------------------
    # loop over patients
    for ptid in dicomDb.getPatientIds():
        print("staring with Patient %s" % (ptid))
        # get patient by ID
        myPatient = dicomDb.getPatient(ptid)
        # loop over RTStructs of this patient
        for myStructUID in myPatient.getRTStructs():
            print("Starting with RTStruct %s" % myStructUID)
            # Get RTSTRUCT by SOP Instance UID
            myStruct = myPatient.getRTStruct(myStructUID)
            # Get CT which is referenced by this RTStruct, and is linked to the same patient
            # mind that this can be None, as only a struct, without corresponding CT scan is found
            myCT = myPatient.getCTForRTStruct(myStruct)
            structfile = myStruct.getFileLocation()
            struct = pydicom.dcmread(structfile)
            SOPInstanceUID = struct.SOPInstanceUID
            patient = 'QIAW_'+ ptid + '_' + SOPInstanceUID
            # generate a fixed folder structure for each patient
            if not os.path.exists(os.path.join(datasetDir,patient)):
                os.makedirs(os.path.join(datasetDir,patient),0o755)
            if not os.path.exists(os.path.join(datasetDir,patient,'dicom','Image')): # DICOM images
                os.makedirs(os.path.join(datasetDir,patient,'dicom','Image'))
            if not os.path.exists(os.path.join(datasetDir,patient,'dicom','STRUCT')): # DICOM-RT
                os.makedirs(os.path.join(datasetDir,patient,'dicom','STRUCT'))
            if not os.path.exists(os.path.join(datasetDir,patient,'SEG')): # DICOM-SEG
                os.makedirs(os.path.join(datasetDir,patient,'SEG'))
            if not os.path.exists(os.path.join(datasetDir,patient,'Mask_Nrrd')): # Binary mask (nrrd) for specific ROIs 
                os.makedirs(os.path.join(datasetDir,patient,'Mask_Nrrd'))
            if not os.path.exists(os.path.join(datasetDir,patient,'Volume_Nrrd')): # Image volume (nrrd)
                os.makedirs(os.path.join(datasetDir,patient,'Volume_Nrrd'))            
            if not os.path.exists(os.path.join(datasetDir,patient,'Json')): # Metadata of segmentation
                os.makedirs(os.path.join(datasetDir,patient,'Json'))
            if not os.path.exists(os.path.join(datasetDir,patient,'DicomSR')): # DICOM-SR
                os.makedirs(os.path.join(datasetDir,patient,'DicomSR'))
            #------------------------------------------------------------  
            # Only executing the following codes when we have both DICOM images and associated RTSTRUCT files
            if myCT is not None:
                # copy RTSTRUCT file to tmp folder as 'struct.dcm'
                shutil.copy2(myStruct.getFileLocation(),os.path.join(datasetDir,patient,'dicom','STRUCT','struct.dcm'))
                # copy DICOM slices to tmp folder as 'struct.dcm'
                slices = myCT.getSlices()
                for i in range(len(slices)):
                    shutil.copy2(slices[i],os.path.join(datasetDir,patient,'dicom','Image',str(i)+".dcm"))
           
                # catch directories
                inputImageDir = os.path.join(datasetDir,patient,'dicom','Image') 
                inputRtDir = os.path.join(datasetDir,patient,'dicom','STRUCT')
                MaskDir = os.path.join(datasetDir,patient,'Mask_Nrrd')
                VolumeDir = os.path.join(datasetDir,patient,'Volume_Nrrd')
                JsontemplateDir = os.path.join(datasetDir)
                JsonexportDir = os.path.join(datasetDir,patient,'Json')
                DicomSRDir = os.path.join(datasetDir,patient,'DicomSR')
                # catch files
                Jsonfile = os.path.join(JsonexportDir,'metadata.json')
                try:
                    # convert DICOM image to ITK nrrd images
                    image = dcmImage2Nrrd(inputImageDir,patient,VolumeDir)
                    # convert ROIs in RTSTRUCT files to ITK nrrd images
                    roi = dcmRT2Nrrd(inputRtDir,inputImageDir,MaskDir,ROIname)

                #-----------------START: NOT USED FOR THIS SCRIPT-------------
                    # # # ------------remove GTV-1auto.nrrd in RIDER --------
                    # # RIDER_auto = os.path.join(MaskDir,'GTV-1auto.nrrd')
                    # # if RIDER_auto in mask_list:
                    # #     os.remove(os.path.join(MaskDir, RIDER_auto))
                    # # mask_list = glob.glob(os.path.join(MaskDir,'*'))
                    # # # ---------------------------------------------------

                    # # Copy Json file to sub-dir per patient
                    # SEGJsoner(JsontemplateDir,JsonexportDir)
                    # workingDir = os.path.join(datasetDir,patient)
                    # SEGfile = patient + '_SEG.dcm'
                    # mask_list = os.listdir(MaskDir)
                    # #----------------------------------------------------

                    # # Change the mask list to fit the requirement of dcmqi docker
                    # for i in range(len(mask_list)):
                    #     mask_list[i] = '/tmp/Mask_Nrrd/'+mask_list[i]
                    # label_list = ','.join(map(str,mask_list))
                    # # ----------------------------------------------------
                    # # Pack ITK image to SEG dcm
                    # dcmNrrds2SEG(label_list,inputImageDir,Jsonfile,SEGfile,workingDir)
                    # # Generate Dicom-SR
                    
                    # # In this version, we used absolute path of pyradiomics-dcm
                    # # pyardiomics/lab/pyradiomics-dcm
                    # pyradiomicsdcmfile = '../pyradiomics/labs/pyradiomics-dcm/pyradiomics-dcm.py'
                    # featureDict = '../pyradiomics/labs/pyradiomics-dcm/resources/featuresDict_IBSIv7.tsv'
                    # paramsyaml = '../pyradiomics/labs/pyradiomics-dcm/Pyradiomics_Params.yaml'
                    # inputSEG = os.path.join(datasetDir,patient,'SEG',('%s_SEG.dcm' %patient))
                    # try:
                    #     ExportDicomSR(pyradiomicsdcmfile,inputImageDir,inputSEG,DicomSRDir,featureDict,paramsyaml)
                    # except:
                    #     print('Error: failed to export DICOM-SR')
                    # # ---------------------------------------
                    # # ----------Not Used---------------------
                    # ## unpack SEG dcm to ITK image
                    # # dcmSEGToNRRDs(SEGfile, workingDir,ptid)
                    # # ---------------------------------------      
                #-----------------END: NOT USED FOR THIS SCRIPT-------------               
                    roilist = glob.glob(os.path.join(MaskDir,'*.nrrd'))
                    for label in roilist:
                        df_NRRD = pd.DataFrame({'ID':[patient+'_'+roi],'Image':[image],'Mask':[label]})
                        # df_NRRD = pd.DataFrame({'ID':[patient+'_'+roi+'_'+dataset_name],'Image':[image],'Mask':[label]})
                        df_combine = pd.concat([df_combine,df_NRRD],axis=0)
                    break
                except:
                    logging.warning('Patient %s does not have a matched ROI  %s:' % (ptid,ROIname))
    return df_combine

if __name__ == '__main__':
  # Specify ROI
  ROIname = ['[G][T][V][-][1]','[G][T][V][p]']
  walk_dir = 'Data'
  dataset_list = glob.glob(os.path.join(os.getcwd(),walk_dir,'*'))
  df = pd.DataFrame()
  for datasetDir in dataset_list:
    df_tmp = main()
    df = pd.concat([df,df_tmp],axis= 0)
  # save the path of image and binary mask in a csv table
  df.to_csv(os.path.join(os.getcwd(),walk_dir,'QIAW_casetable.csv'),index = False)
  # radiomics extraction in the batch mode
  batchProcessingWithPandas.main()

