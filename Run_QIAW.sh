#!/bin/sh

# -----------------------------------
echo "----------Download data from XNAT----------------------------------------------"
echo "Create a directory for RIDER dataset"
if [ ! -d ./Data/RIDER ] 
then
    mkdir -p ./Data/RIDER
fi
cd ./QIAW
python QIAW_XNAT.py
echo "Data download is done !!!"

# move data from RIDER to RIDER_TEST/RETEST
mv Data/RIDER/*RETEST Data/RIDER_RETEST/
mv Data/RIDER/*TEST Data/RIDER_TEST/

echo "-------------------------------------------------------------------------------"
echo "-------------Start processing--------------------------------------------------"
# using script
python QIAW_mainscript.py
echo "Radiomic features, DICOM-SEG, DICOM-SR have been generated successfully !!!!"
## alternative using Docker radiomics/pyradiomics:CLI
# create a dir to store radiomic features
echo "-------------------------------------------------------------------------------"
echo "Create a directory to store radiomic features"
if [ ! -d ./Data/RF_docker ] 
then
    mkdir -p ./Data/RF_docker
fi

# convert original path to /tmp
echo "Calculation radiomics using PyRadiomics Docker (CLI)"
python QIAW_path2tmp.py
sudo docker run -v /home/ubuntu/QIAW:/tmp radiomics/pyradiomics:CLI /tmp/Data/QIAW_casetable_CLI.csv -p /tmp/ParamsSettings/Pyradiomics_Params.yaml  --out rf.csv --out-dir /tmp/Data/RF_docker
echo "------------------------------All QIAW processing done--------------------------"