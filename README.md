## Build Status

| Linux                          | macOS                         | Windows                       |
|--------------------------------|-------------------------------|-------------------------------|
| Passed                         | Under Development             | Under Development             |

# FAIR-QIAW
## Findable(F), Accessible(A), Interoperability(I) and Reuse(R) - Quantitative(Q) Imaing(I) Analysis(A) Workflow (W)

Welcome to FAIR-Quantitative Imaging Analysis Workflow (FAIR-QIAW) repository.
FAIR-QIAW provides a suggested analysis workflow for quantitative imaging including not only radiomics, but deep learning-based radiomics. The process starts from DICOM file that is the most commonly used medical image format in clinical practice and ends with multiple quantitative imaging features and related analysis for specific outcome. 

## Citation
The publication of FAIR-QIAW is coming soon. Please cite the webpage when you use it for academic research.

## *** Warning

The current version of FAIR-QIAW is developed on Ubuntu 18.04. The program should be compatible for Ubuntu 16.04 or higher. For other Linux OS, only the packages installation way is different. For Windows and macOS, the developers have not test yet, but will support in future.

## Disclaimer

FAIR-QIAW is still under development. Although we have tested and evaluated the workflow under many different situations, errors and bugs still happen unfortunately. Please use it cautiously. If you find any, please contact us and we would fix them ASAP.

## Quick Start
The easiest accessible way to use FAIR-QIAW is via the AWS instance we share to public. There you could experience the workflow and look into the results in each step,to figure out how FAIR-QIAW works. In this document, we would explain the FAIR-QIAW step by step, and provide instruction about how to implement on AWS instance and your local computer.


### Prerequisites 

FAIR-QIAW dependents on several main tools and packages that are listed below.

1. [Anaconda3](https://www.anaconda.com/download) python version 3, which includes python and hundreds of popular data science packages and the conda package and virtual environment manager for Windows, Linux, and MacOS. Note that, Python >= 3.6.
2. [Docker](https://docs.docker.com) Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.
3. [Pyradiomics](https://github.com/Radiomics/pyradiomics) - radiomics extractor.
4. [plastimatch](https://www.plastimatch.org/) - is an open source software for image computation. Its focus is high-performance volumetric registration of medical images, such as X-ray computed tomography (CT), magnetic resonance imaging (MRI), and positron emission tomography (PET).
5. [dcmqi](https://github.com/QIICR/dcmqi) - Convert certain types of quantitative image analysis results into standardized DICOM form.


### Installation

The bash script is able to finish the installation

Execute:
```
bash ./AWS/AWS_QIAWinitializer.sh
```
Note that, the initializer is only tested on empty AWS EC2 ubuntu 18.04 instance. To install on local PC, please do it step by step following the bash script.

### Getting Started

Execute:
```
bash Run_QIAW.sh

```
Note that, the implementation script is only tested on empty AWS EC2 ubuntu 18.04 instance in the current version. Please do it step by step following the bash script.

## License

FAIR-Radiomics may not be used for commercial purposes. This package is freely available to browse, download, and use for scientific and educational purposes as outlined in the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).

## Developers
 - [Zhenwei Shi](https://github.com/zhenweishi)<sup>1</sup>
 - [Leonard Wee]<sup>1</sup>
 - [Andre Dekker]<sup>1</sup>

 
<sup>1</sup>Department of Radiation Oncology (MAASTRO Clinic), GROW-School for Oncology and Development Biology, Maastricht University Medical Centre, The Netherlands.


### Contact
We are happy to help you with any questions. Please contact Zhenwei Shi.
Email: zhenwei.shi@maastro.nl

We welcome contributions to FAIR-QIAW.