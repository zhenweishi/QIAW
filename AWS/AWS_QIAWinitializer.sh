#!/bin/sh

echo "Step-1:"
echo "-------- initial update-------"
sudo apt-get update

echo "Step-2:"
echo "-------- install Anaconda3-----" 
cd /tmp
curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
echo "Anaconda3 installing ..."
# install anaconda needs some manual operations
bash Anaconda3-2019.03-Linux-x86_64.sh
# remove anaconda installation bash file
sudo rm Anaconda3-2019.03-Linux-x86_64.sh
# add anaconda3 to the environment variable path 
export PATH=~/anaconda3/bin:$PATH

echo "Step-3:"
echo "--------- install Docker-------"
sudo snap install docker     # version 18.06.1-ce

echo "Step-4:"
echo "--------- install gcc-------"
sudo apt install gcc

echo "Step-5:"
echo "--------- install latest pyradiomics-------"
cd ~
git clone https://github.com/Radiomics/pyradiomics.git
cd ~/pyradiomics
python -m pip install -r requirements.txt
python setup.py install

echo "Step-6:"
echo "--------- install plastimatch package-------"
sudo apt-get install plastimatch

echo "Step-7:"
echo "-------- install dcmqi via docker-------"
#sudo docker pull qiicr/dcmqi
sudo apt install dcm2niix

echo "Step-8:"
echo "--------- install FAIR-QIAW package-------"
cd ~
# now FAIR-QImaging is private repository, username: zhenweishi88@gmail.com, password: Shizhenwei08
git clone https://gitlab.com/zhenweishi/QIAW.git
cd ./QIAW
# install FAIR-QIAW dependencies
python -m pip install -r QIAW_requirements.txt

# ----------- do not have other options ------
wget https://github.com/QIICR/dcmqi/releases/download/v1.2.1/dcmqi-1.2.1-linux.tar.gz
tar -xzvf dcmqi-1.2.1-linux.tar.gz
mv dcmqi-1.2.1-linux dcmqi
rm dcmqi-1.2.1-linux.tar.gz
export PATH="$(pwd)/dcmqi/bin":$PATH

echo "Step-9:"
echo "--------- install xnat for data downloading-------"
pip install xnat