import os, glob,shutil

mainpath = './Data'
dir_list = ['MMD', 'Lung1','RIDER_TEST', 'HN1', 'RIDER_RETEST']
for i in range(len(dir_list)):
    sub_dir = os.listdir(os.path.join(mainpath,dir_list[i]))
    for j in range(len(sub_dir)):
        subsub_dir = os.path.join(mainpath,dir_list[i],sub_dir[j])
        if os.path.isdir(subsub_dir):
#             print(os.path.join(subsub_dir,'dicom'))
            shutil.rmtree(os.path.join(subsub_dir,'dicom'))
            print('remove %s, done' % os.path.join(subsub_dir,'dicom'))